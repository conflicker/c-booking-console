#include<iostream>
#include<fstream>
#include<conio.h>
#include<iomanip>
#include<stdio.h>
#include<string.h>

using namespace std;
int menu();
class Booking
{
      private:
              int bookid;
              char fullname[20];
              char arrivalDate[20];
			  char departureDate[20];
			  float amount;
			  char status[20];
      protected:
                int newId();
                void tableHeader();
      public:
             void bookForm();
			 void saveBook();
			 void bookingList();
			 void bookingListRow();
			 void editRecord();
			 void deleteRecord();
             
};

int Booking::newId()
{
    ifstream fin;
    Booking temp;
    int id=0;
    fin.open("booking.txt",ios::in|ios::binary);
    if(!fin)
            return(id+1);
    else
    {
        fin.read((char*)&temp,sizeof(temp));
        while(!fin.eof())
        {
         id=temp.bookid;
         fin.read((char*)&temp,sizeof(temp));
        }
        id++;
        return(id);
    }
}

void Booking::bookForm()
{
	cout << "Full name: ";
	fflush(stdin);
	gets(fullname);

	cout << "Arrival date: ";	
	gets(arrivalDate);

	cout << "Departure date: ";	
	gets(departureDate);

	cout << "Amount: ";
	cin >> amount;

	cout << "Status: ";
	cin >> status;

	bookid = newId();

}

void Booking::saveBook()
{
	ofstream fout;
    fout.open("booking.txt",ios::out|ios::app|ios::binary);
	if(!fout){
        cout<<"Database not found.";
	}else{
       fout.write((char*)this,sizeof(*this));
	}
    
	fout.close();
}

void Booking::bookingList()
{
	 ifstream fin;
     fin.open("booking.txt",ios::in|ios::binary);
     if(!fin)
             cout<<"File not found";
     else
     {
         tableHeader();
         fin.read((char*)this,sizeof(*this));
         while(!fin.eof())
         {
          bookingListRow();
          fin.read((char*)this,sizeof(*this));
         }
     }
     fin.close();
}

void Booking::tableHeader()
{
	cout << left;
	cout << "\n" << setw(10) << "Book ID" << setw(20) << "Name" << setw(20) << "Arrival" << setw(20) << "Departure" << setw(10) << "Amount" << setw(10) << "Status\n";
}

void Booking::bookingListRow()
{
     cout<<left;
     cout<<"\n" << setw(10) << bookid << setw(20) << fullname << setw(20) << arrivalDate << setw(20) << departureDate << setw(10) << amount << setw(10) << status;
}

void Booking::editRecord()
{
	int id, row = 0;
	fstream file;
	file.open("booking.txt",ios::in|ios::out|ios::ate|ios::binary);
	cout<<"\nEnter booking id: ";
	cin >> id;
	file.seekg(0);

	if(!file) {
		cout << "Database not found" << endl;
	}else {
		file.read((char*)this,sizeof(*this));
		while(!file.eof())
         {
           row++;
           if(bookid==id)
           {
             tableHeader();
             bookingListRow();
             cout<<"\n\nRe-enter booking information:\n";

             cout<< "Full name: ";
             fflush(stdin);
             gets(fullname);

             cout<< "Arrival Date: ";
             gets(arrivalDate);

			 cout << "Departure Date: ";
			 gets(departureDate);

			 cout << "Amount: ";
			 cin >> amount;

			 cout << "Status: ";
			 cin >> status;


             file.seekp((row-1)*sizeof(Booking),ios::beg);
             file.write((char*)this,sizeof(*this));
             break;
           }

           file.read((char*)this,sizeof(*this));
         }

		if(file.eof()){
			cout<< "Record not found";
		}

	}

	file.close();
}

void Booking::deleteRecord()
{
	ifstream fin;
	ofstream fout;
	int id;
	char x;
	fin.open("booking.txt",ios::in|ios::binary);
	fout.open("tempfile.txt",ios::out|ios::app|ios::binary);
	cout<<"Enter book id: ";
	cin>>id;
	if(!fin)
		 cout<<"File not found";
	else
	{
	 fin.read((char*)this,sizeof(*this));
	 while(!fin.eof())
	 {
	  if(this->bookid==id)
	  {
		cout<<"Record you want to delete is:\n\n";
		tableHeader();
		bookingListRow();
		cout<<"\n\nAre you sure you want to delete this record(y/n): ";
		fflush(stdin);
		cin>>x;
		if(x=='n'){
			fout.write((char*)this,sizeof(*this));
		}else{
			cout<<"\nRecord has been successfully deleted.";
		}
	  }
	  else
		  fout.write((char*)this,sizeof(*this));
		  fin.read((char*)this,sizeof(*this));
	 }
	 fin.close();
	 fout.close();     


	system("erase booking.txt");
	getch();
	system("rename tempfile.txt booking.txt");


	}

}

int menu()
{
	system("cls");	

	cout << "[1]" << " New Booking" << endl;
	cout << "[2]" << " Booking List" << endl;
	cout << "[3]" << " Booking Update" << endl;
	cout << "[4]" << " Delete Record" << endl;
	cout << "[5]" << " Exit" << endl;
	cout << "Enter your choice: ";
	int ch;
	cin >> ch;

	return(ch);
}

int main()
{
	Booking b;
	int ch;

    while(1)
    {
		   
           ch = menu();
           switch(ch)
           {
                     case 1:
                            b.bookForm();
							b.saveBook();
							cout << "Record has been successfully saved!" << endl;
							break;
					 case 2: b.bookingList();
							break;
					 case 3: 
							b.editRecord();
							cout << "Record has been successfully saved!" << endl;
							break;
					 case 4: 
							b.deleteRecord();
							break;
                     case 5:
                            exit(0);	
							break;
                     default:
                            cout<<"Enter Valid choice";							
							
           }
           getch();
    }
}